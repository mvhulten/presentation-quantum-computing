\documentclass[bigger,xcolor={dvipsnames}]{beamer}
\usefonttheme{professionalfonts}

\usepackage{stilo_hulten}
\mode<presentation>
{ \usetheme{boxes} }
\usepackage{times}
\usepackage{boxedminipage,shadow,fancybox}
\usepackage{rotating}
\usepackage{tikz,transparent}
\usepackage{adjustbox}
\usepackage{ulem}
\usepackage{csquotes}

\newcommand{\thickfrac}[2]{\genfrac{}{}{3pt}{}{#1}{#2}}

% Bibliografie
% 
\usepackage{cite}
\DeclareRobustCommand{\DutchName}[4]{#2~#1}

%\usetheme[compress]{Berlin}
\usetheme{Madrid}
\setbeamertemplate{navigation symbols}{}

\vspace*{-5mm}
\title{Quantum computing}
\subtitle{Fundamentals and applications}
\author{Marco van Hulten}
\date{20 April 2022}

\begin{document}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\usebackgroundtemplate{}

\begin{frame}
\maketitle
\end{frame}

\section{Introduction}
\begin{frame}{Why quantum computing?}
\begin{itemize}
  \item Some specific algorithms are much faster on a quantum computer
  \item Quantum teleportation
  \item Quantum cryptography
\end{itemize}
\end{frame}

\begin{frame}{How does it work?}
\vfill
{\hfill bit \ $\longrightarrow$ \ {\bf qu}bit \hfill}
\vfill
\end{frame}

\begin{frame}{Quantum mechanics}
    \center
    \vskip -2mm
    \includegraphics[height=.95\textheight]{schrodinger.png}
\end{frame}

\begin{frame}{Quantum mechanics}
In classical mechanics only \emph{eigenstates} exist, e.g.
\begin{itemize}
  \item $(\mathbf{x}, \mathbf{p})$, precisely determined
  \item a \emph{bit} that is either 0 or 1
\end{itemize}

\pause
\vfill
In quantum mechanics there are also superposition states, e.g.
\begin{itemize}
  \item $(\mathbf{x}, \mathbf{p})$ with $\Delta x\cdot\Delta p \ge \hbar/2$,\\
		probability to find system at position $\mathbf{x}_0$ when measured:\\
		$|\langle x_0 | \Psi \rangle|^2$
  \item a \emph{qubit} that is in a superposition state of $|0\rangle$ and $|1\rangle$
\end{itemize}

%\vfill
%(Mixed states, incoherent states or statistical ensembles are a different story.)
\end{frame}

\begin{frame}{Quantum information}
%\includegraphics[width=\linewidth]{.jpg}
Units of information:
\begin{itemize}
  \item A bit has one of two (classical) states -- discrete states
  \item A qbit has infinite possible (quantum) states -- $n$ qubits span an
		$n$-dimensional Hilbert space
\end{itemize}
\pause
\vfill
Computation:
\begin{itemize}
  \item Apply operations on state vectors (can be superpositions)
  \item Quantum teleportation
  \item Cryptological algorithms
\end{itemize}
\end{frame}

\section{Hardware}
\begin{frame}{Problems and solutions}
The problems:
\begin{itemize}
  \item Isolation (decoherence)
  \item No-cloning
\end{itemize}

\vfill
\pause
Potential solutions:
\begin{itemize}
  \item Error correction
  \item Quantum error correction (entangled qubits)
  \item Entanglement
  \item Try to isolate
  \item Try to act on very short timescales \cite{agueny2020}
\end{itemize}
\end{frame}

\section{Quantum algorithms}
\begin{frame}{Quantum logical gates}
% Needed for circuits
\begin{itemize}
  \item Hadamard (prepare superposition state):
      \(H = \frac{1}{\sqrt{2}}\begin{pmatrix} 1 & 1\\ 1 &-1 \end{pmatrix}\)
  \item analog of classical XOR:
      \vskip -5mm
        \(\hskip 35mm CX = \begin{pmatrix}1&0&0&0\\0&1&0&0\\0&0&0&1\\0&0&1&0\end{pmatrix}\)
            \vskip 5mm
  \item bit-flip:   $X = \begin{pmatrix} 0 & 1\\ 1 & 0 \end{pmatrix}$,
                   $Y = \begin{pmatrix} 0 &-i\\ i & 0 \end{pmatrix}$,
  \item phase-flip: $Z = \begin{pmatrix} 1 & 0\\ 0 &-1 \end{pmatrix}$,
  %\item Phase shift
\end{itemize}
\end{frame}

\begin{frame}{Quantum states}
    Choose a basis $(|0\rangle, |1\rangle) \dot= \left(
    \begin{pmatrix} 1 \\ 0 \end{pmatrix},
        \begin{pmatrix} 0 \\ 1 \end{pmatrix} \right)$.

These span a 2-D Hilbert space; besides the eigenstates, two important states
(in quantum algorithms) are
\begin{align}
    |+\rangle &= \frac{|0\rangle + |1\rangle}{\sqrt 2}\,,\\
    |-\rangle &= \frac{|0\rangle - |1\rangle}{\sqrt 2}\,.
\end{align}

Use Hadamard transform to go between eigen- and superposition states.
\end{frame}

\begin{frame}{Teleportation}
    \vskip -2mm
    \includegraphics[width=.95\textwidth]{spooky.jpg}
\end{frame}

\begin{frame}{Teleportation}
Say we want to instantly send information $|\psi\rangle_A$ from Alice to Bob.
%Relevant information will be in $|\psi\rangle_A$ at Alice' location,
%and $|\psi\rangle_B$ at Bob's location.
\begin{enumerate}
  \item Entangle two systems $|\xi\rangle_X$ and $|\phi\rangle_B$
      $\longrightarrow |\Psi_1\rangle_{XB}$
  \item Bob takes a holiday to Risa (22\;ly from Earth), bringing along system
      $B$
  \item Alice wants to send information to Bob (but doesn't want to wait
		44\;yr for an answer)
  \item She encodes the information in $|\psi\rangle_A$ and entangles it with
      $|\Psi_1\rangle_{XB}$
      %(original state $|\xi\rangle_X$ does not exist anymore)
        $\longrightarrow |\Psi_2\rangle_{AXB}$
    \item Bob measures (the relevant observable for) $|\phi\rangle_B$ from which
        he can deduce $|\psi\rangle_A$
\end{enumerate}

\vfill
Entanglement can be done with quantum logical gates like operators CX and H.
\end{frame}

\begin{frame}{Encryption}
    \vskip -2mm
    \includegraphics[height=.95\textheight]{enigma.jpg}
\end{frame}

\begin{frame}{Encryption}
Shor's algorithm breaks (classical) prime number based algorithms like RSA.
Solutions are new algorithms:
\begin{itemize}
  \item \sout{RSA}
  \item BB84 \small\cite{bennett2014}
  \item E91 \small[Ekert, 1991]
  \item NTRU \small[Hoffstein, Pipher and Silverman, 1996]
  \item NTRU Prime \small\cite{bernstein2016}
\end{itemize}
Because of \emph{no-cloning} messages are not interceptable.

\vfill
OpenBSD includes an NTRU Prime algorithm (combined with Ed25519 to bar potential
NTRU Prime attacks).
\begin{itemize}
  \item In OpenBSD 7.0 it got included in IPSEC and OpenSSH
  \item Last week it became the default key exchange method in OpenSSH 9.0
		($\rightarrow$ OpenBSD 7.1)
\end{itemize}
\end{frame}

\begin{frame}{Why now?}
There are no powerful quantum computers...
\pause
$\mathbf{yet.}$

\vfill
From the OpenSSH 9.0 changelog:

\begin{displayquote}
   We are making this change now (i.e.\ ahead of cryptographically
   relevant quantum computers) to prevent ``capture now, decrypt
   later'' attacks where an adversary who can record and store SSH
   session ciphertext would be able to decrypt it once a sufficiently
   advanced quantum computer is available.
\end{displayquote}
\end{frame}

% Bib(La)TeX with correct use of voorvoegsels in index
%
\DeclareRobustCommand{\DutchName}[4]{#3 #4~#1}
\begin{frame}{}
\bibliography{misc_papers}{}
\bibliographystyle{apalike}
\end{frame}

\end{document}
